# Touch ID Web Authentication

TouchIDAuth is a Safari extension which allow a web application to ask for the
user a second authentication using the Apple Touch ID technology.

The Touch ID is not yet accessible directly from Safari (excepted through
Apple Pay), until Apple implements it, this extension allows you to use it now.

## WARNING

This is not yet production ready, the security layer has to be done.

## Privacy & Security

This extension can't access your keys, password or fingerprints thanks to the
so called [Secure Enclave](https://support.apple.com/en-au/HT204587) which
holds them secruely.

This extension is requesting a Touch ID Authentication through [the Local
Authentication framework](https://developer.apple.com/documentation/localauthentication)
(which uses the Secure Enclave).

## TODO

 - [x] Implement the minimal code to authenticate with Touch ID
 - [ ] Add security layer (JavaScript event encrypted data) [Issue #1](https://github.com/zedtux/touchIDAuth/issues/1)
 - [ ] Design events protocol preventing workarounding the extension [Issue #2](https://github.com/zedtux/touchIDAuth/issues/2)
 - [ ] Build a JavaScript library implementing the extension in a web app [Issue #3](https://github.com/zedtux/touchIDAuth/issues/3)
 - [ ] Add automated tests

## Testing

In order to test it:

 - Open the project with Xcode
 - Build the Safari extension (Select "Touch ID Auth Extension" from the Schemes)
 - Run the project within safari
 - When Safari poped up
   - Safari > Preferences > Advanced > Check the "Show Develop menu in menu bar"
   - Develop > Allow Unsigned Extensions
   - Safari > Preferences > Extensions > Enable the "Touch ID Auth" extension
   - Open the `index.html` from the `Touch ID Auth IntegrationTest` folder

It should ask you for authentication using the Touch ID sensor if you have one.

You will need to have the page served by a web server because Safari doesn't allow
an extension to run for `file:///` URLs.

I'm doing so using [this Nginx Docker image](https://hub.docker.com/_/nginx/) in
order to server it, but you can also install and run a local web server instance.

If you'd like to use Docker like I do, from this project folder, run the following command:

```
$ docker run --rm \
             --name touchIDAuth \
             --volume "$PWD/Touch ID Auth IntegrationTest/":/usr/share/nginx/html:ro \
             --publish "8080:80" \
             nginx
```

Now you can access the testing page at http://localhost:8080.
