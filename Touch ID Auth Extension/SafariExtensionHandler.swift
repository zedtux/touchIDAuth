//
//  SafariExtensionHandler.swift
//  Touch ID Auth Extension
//
//  Created by Guillaume Hain on 30/12/2017.
//  Copyright © 2017 Zedroot. All rights reserved.
//

import SafariServices
import LocalAuthentication // For TouchID mechanism

class SafariExtensionHandler: SFSafariExtensionHandler {
    
    override func messageReceived(withName messageName: String, from page: SFSafariPage, userInfo: [String : Any]?) {
        // This method will be called when a content script provided by your extension calls safari.extension.dispatchMessage("message").
        page.getPropertiesWithCompletionHandler { properties in
            NSLog("The extension received a message (\(messageName)) from a script injected into (url: \(String(describing: properties?.url)), title: \(String(describing: properties?.title)) with userInfo (\(userInfo ?? [:]))")

            if messageName == "TouchIDAuthRequest" {
                // Executes the requestTouchIDAuth function passing the web site URL and the page object
                self.requestTouchIDAuth(withUrl: String(describing: properties?.url), from: page)
            }
        }
    }

    // Ensure a Touch ID sensor is available and request the user to authenticate
    // Finally emit a TouchIDAuthRequestResponse event with the result of authentication
    func requestTouchIDAuth(withUrl url: String, from page: SFSafariPage) {
        let authenticationContext = LAContext()
        
        var error:NSError?
        
        guard authenticationContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            NSLog("No Touch ID sensor avaialble")
            return
        }

        NSLog("Touch ID sensor avaialble!")
        
        authenticationContext.evaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            localizedReason: "authenticate to \(url)",
            reply: { [unowned self] (success, error) -> Void in
                
                if ( success ) {
                    
                    // Fingerprint recognized
                    NSLog("Fingerprint recognized!")
                    page.dispatchMessageToScript(withName: "TouchIDAuthRequestResponse", userInfo: ["auth": "success"])
                    
                } else {
                    
                    // Check if there is an error
                    if let errorObj = error {
                        
                        NSLog("Fingerprint NOT recognized: \(errorObj.localizedDescription)")
                        page.dispatchMessageToScript(withName: "TouchIDAuthRequestResponse", userInfo: ["auth": "fail"])
                        
                    }
                    
                }
                
        })
    }
    
    override func validateToolbarItem(in window: SFSafariWindow, validationHandler: @escaping ((Bool, String) -> Void)) {
        // This is called when Safari's state changed in some way that would require the extension's toolbar item to be validated again.
        validationHandler(true, "")
    }
    
    override func popoverViewController() -> SFSafariExtensionViewController {
        return SafariExtensionViewController.shared
    }

}
