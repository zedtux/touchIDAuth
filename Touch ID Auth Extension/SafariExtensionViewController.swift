//
//  SafariExtensionViewController.swift
//  Touch ID Auth Extension
//
//  Created by Guillaume Hain on 30/12/2017.
//  Copyright © 2017 Zedroot. All rights reserved.
//

import SafariServices

class SafariExtensionViewController: SFSafariExtensionViewController {
    
    static let shared = SafariExtensionViewController()

}
