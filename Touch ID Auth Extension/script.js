// Wait for the web page to be loaded
document.addEventListener("DOMContentLoaded", function(event) {
    console.log("script.js: DOMContentLoaded fired");

    // Listen to the TouchIDAuthRequest event used to request the user to authenticate using a Touch ID sensor
    document.addEventListener("TouchIDAuthRequest", function(event) {
        console.log("script.js: TouchIDAuthRequest event triggered!");
        // Fire the SafariExtensionHandler messageReceived function passing the messageName "TouchIDAuthRequest"
        safari.extension.dispatchMessage("TouchIDAuthRequest");
    });

    // Handle the messages emitted from SafariExtensionHandler
    function handleMessage(event) {
        console.log("script.js: Event name:", event.name);
        console.log("script.js: Message:", event.message);
        
        if (event.name === "TouchIDAuthRequestResponse") {
            console.log("script.js: Building 'TouchIDAuthResponse' event ...");
            var event = new CustomEvent("TouchIDAuthResponse", { detail: event.message });
            console.log("script.js: Firing 'TouchIDAuthResponse' event ...");
            document.dispatchEvent(event);
            console.log("script.js: Event 'TouchIDAuthResponse' fired");
        }
    }
    // Listen to emitted from SafariExtensionHandler
    safari.self.addEventListener("message", handleMessage);

    // Emit the TouchIDAuthReady event notifying the web page about the extension
    var event = new Event("TouchIDAuthReady");
    console.log("script.js: Firing TouchIDAuthReady event ...");
    document.dispatchEvent(event);
});
