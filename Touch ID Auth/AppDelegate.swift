//
//  AppDelegate.swift
//  Touch ID Auth
//
//  Created by Guillaume Hain on 30/12/2017.
//  Copyright © 2017 Zedroot. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

